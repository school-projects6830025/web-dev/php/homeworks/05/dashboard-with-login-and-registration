<?php
require_once 'inc/user.php';
/** @var \PDO $db  */

if (!empty($_GET['id'])) {
    $pageTitle = 'Úprava příspěvku';
} else {
    $pageTitle = 'Nový příspěvek';
}

if (empty($_SESSION['user_id'])) {
    $_SESSION['chyba'] = 'Pro úpravu příspěvků na nástěnce musíte být přihlášen(a).';
    require_once 'inc/header.php';
}

$articleId = '';
$postCategory = (!empty($_REQUEST['category']) ? intval($_REQUEST['category']) : '');
$postText = '';

if (!empty($_REQUEST['id'])) {
    $postQuery = $db->prepare('SELECT * FROM articles WHERE article_id=:id LIMIT 1;');
    $postQuery->execute([':id' => $_REQUEST['id']]);
    $article = $postQuery->fetch(PDO::FETCH_ASSOC);
    if ($article === false) {
        $_SESSION['chyba'] = 'Příspěvek neexistuje.';
        require_once 'inc/header.php';
    }

    if ($article['account_id'] != $_SESSION['user_id']) {
        $_SESSION['chyba'] = 'Nemáte oprávnění tento příspěvek upravovat.';
        require_once 'inc/header.php';

    } else if (!empty($_POST['smazat'])) {
        $deleteQuery = $db->prepare("DELETE FROM articles WHERE article_id=:id;");
        $deleteQuery->execute([
            ':id' => $_POST['smazat']
        ]);
        $_SESSION['uspech'] = 'Prispevek byl uspesne smazan.';
        header('Location: index.php');
        exit();

    } else {
        $articleId = $article['article_id'];
        $postCategory = $article['category_id'];
        $postText = $article['text'];
    }
}

$errors = [];
if (!empty($_POST)) {
    if (!empty($_POST['category'])) {
        $categoryQuery = $db->prepare('SELECT * FROM categories WHERE category_id=:category LIMIT 1;');
        $categoryQuery->execute([
            ':category' => $_POST['category']
        ]);

        if ($categoryQuery->rowCount() == 0) {
            $errors['category'] = 'Zvolená kategorie neexistuje!';
            $postCategory = '';
        } else {
            $postCategory = $_POST['category'];
        }

    } else {
        $errors['category'] = 'Musíte vybrat kategorii.';
    }

    $postText = trim(@$_POST['text']);
    if (empty($postText)) {
        $errors['text'] = 'Musíte zadat text příspěvku.';
    }

    if (empty($errors)) {
        if ($articleId) {
            $saveQuery = $db->prepare('UPDATE articles SET category_id=:category, text=:text, account_id=:account WHERE article_id=:id LIMIT 1;');
            $saveQuery->execute([
                ':category' => $postCategory,
                ':text' => $postText,
                ':id' => $articleId,
                ':account' => $_SESSION['user_id']
            ]);

        } else {
            $saveQuery = $db->prepare('INSERT INTO articles (account_id, category_id, text) VALUES (:account, :category, :text);');
            $saveQuery->execute([
                ':account' => $_SESSION['user_id'],
                ':category' => $postCategory,
                ':text' => $postText
            ]);
        }

        header('Location: index.php?category=' . $postCategory);
        exit();
    }
}

include 'inc/header.php';
?>
    <form method="post">
        <input type="hidden" name="id" value="<?php echo $articleId; ?>"/>
        <div class="form-group">
            <label for="category">Kategorie:</label>
            <select name="category" id="category" required
                    class="form-control <?php echo(!empty($errors['category']) ? 'is-invalid' : ''); ?>">
                <option value="">--vyberte--</option>
                <?php
                $categoryQuery = $db->prepare('SELECT * FROM categories ORDER BY name;');
                $categoryQuery->execute();
                $categories = $categoryQuery->fetchAll(PDO::FETCH_ASSOC);
                if (!empty($categories)) {
                    foreach ($categories as $category) {
                        echo '<option value="' . $category['category_id'] . '" ' . ($category['category_id'] == $postCategory ? 'selected="selected"' : '') . '>' . htmlspecialchars($category['name']) . '</option>';
                    }
                }
                ?>
            </select>
            <?php
            if (!empty($errors['category'])) {
                echo '<div class="invalid-feedback">' . $errors['category'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="text">Text příspěvku:</label>
            <textarea name="text" id="text" required
                      class="form-control <?php echo(!empty($errors['text']) ? 'is-invalid' : ''); ?>"><?php echo htmlspecialchars($postText) ?></textarea>
            <?php
            if (!empty($errors['text'])) {
                echo '<div class="invalid-feedback">' . $errors['text'] . '</div>';
            }
            ?>
        </div>

        <div class="row no-gutters justify-content-between">
            <div class="form-row form-group">
                <button type="submit" class="btn btn-primary">uložit...</button>
                <a href="index.php?category=<?php echo htmlspecialchars($postCategory); ?>" class="btn btn-light">zrušit</a>
            </div>
            <div class="form-row form-group">
                <button type="submit" class="btn btn-danger" name="smazat" id="smazat" value="<?php echo $articleId ?>">
                    Smazat
                </button>
            </div>
        </div>
    </form>

<?php
include 'inc/footer.php';