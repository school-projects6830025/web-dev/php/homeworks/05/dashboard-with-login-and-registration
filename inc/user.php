<?php
session_start();
require_once 'db.php';
/** @var \PDO $db */

if (!empty($_SESSION['user_id'])) {
    $userQuery = $db->prepare('SELECT account_id FROM accounts WHERE account_id=:id AND active=1 LIMIT 1;');
    $userQuery->execute([
        ':id' => $_SESSION['user_id']
    ]);
    if ($userQuery->rowCount() != 1) {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_name']);
        header('Location: index.php');
        exit();
    }
}