<?php
  require_once 'inc/user.php';
  include 'inc/header.php';
  /** @var \PDO $db  */

  if (!empty($_GET['category'])) {
    $query = $db->prepare('SELECT
                           articles.*, accounts.name AS user_name, accounts.email, categories.name AS category_name
                           FROM articles JOIN accounts USING (account_id) JOIN categories USING (category_id) WHERE articles.category_id=:category ORDER BY updated DESC;');
    $query->execute([
      ':category'=>$_GET['category']
    ]);
    
  } else {
    $query = $db->prepare('SELECT
                           articles.*, accounts.name AS user_name, accounts.email, categories.name AS category_name
                           FROM articles JOIN accounts USING (account_id) JOIN categories USING (category_id) ORDER BY updated DESC;');
    $query->execute();
  }

  echo '<form method="get" id="categoryFilterForm">
          <label for="category">Kategorie:</label>
          <select name="category" id="category" onchange="document.getElementById(\'categoryFilterForm\').submit();">
            <option value="">--nerozhoduje--</option>';

  $categories=$db->query('SELECT * FROM categories ORDER BY name;')->fetchAll(PDO::FETCH_ASSOC);
  if (!empty($categories)) {
    foreach ($categories as $category) {
      echo '<option value="'.$category['category_id'].'"';
      if ($category['category_id']==@$_GET['category']) {
        echo ' selected="selected" ';
      }
      echo '>'.htmlspecialchars($category['name']).'</option>';
    }
  }

  echo '  </select>
          <input type="submit" value="OK" class="d-none" />
        </form>';

  $articles = $query->fetchAll(PDO::FETCH_ASSOC);
  if (!empty($articles)) {
    echo '<div class="row">';
    foreach ($articles as $article) {
      echo '<article class="col-12 col-md-6 col-lg-4 col-xxl-3 border border-dark mx-1 my-1 px-2 py-1">';
      echo '  <div><span class="badge badge-secondary">'.htmlspecialchars($article['category_name']).'</span></div>';
      echo '  <div>'.nl2br(htmlspecialchars($article['text'])).'</div>';
      echo '  <div class="small text-muted mt-1">';
                echo htmlspecialchars($article['user_name']);
                echo ' ';
                echo date('d.m.Y H:i:s',strtotime($article['updated']));

                if (!empty($_SESSION['user_id']) && $_SESSION['user_id'] == $article['account_id'] ) {
                  echo ' - <a href="edit.php?id='.$article['article_id'].'" class="text-danger">upravit</a>';
                }

      echo '  </div>';
      echo '</article>';
    }
    echo '</div>';
  } else {
    echo '<div class="alert alert-info">Nebyly nalezeny žádné příspěvky.</div>';
  }

  if (!empty($_SESSION['user_id'])) {
    echo '<div class="row my-3">
            <a href="edit.php'.(!empty($_GET['category'])?'?category='.htmlspecialchars($_GET['category']):'').'" class="btn btn-primary">Přidat příspěvek</a>
          </div>';
  }

  include 'inc/footer.php';